﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;

namespace figures.figures
{
    public class FiguresList
    {
        private List<Figures> list;

        public FiguresList()
        {
            list = new List<Figures>();
        }

        ~FiguresList()
        {
            list.Clear();
        }
        
        public void Add(Figures f)
        {
            list.Add(f);
        }
        
        public void DelLast()
        {
            list.RemoveAt(list.Count - 1);
        }
        
        public void AllInfo()
        {
            if (list.Count == 0)
            {
                Console.WriteLine("List is empty");
                return;
            }
            foreach (Figures f in list)
            {
                if (f.type == 'c')
                    Console.Write("Circle: ");
                else if(f.type == 'e')
                    Console.Write("Ellipse: ");
                else
                    Console.Write("Polygon: ");
                
                Console.Write("Area = " + f.Area() + " |   Perimeter = "
                                  + f.Perimeter() + " |   Center of Mass ("+f.CenterOfMass().X
                                  + ";"+ f.CenterOfMass().Y +")\n");
            }
        }
    }
}