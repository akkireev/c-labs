﻿using System;
using System.Reflection;
using System.Runtime.Remoting.Messaging;

namespace figures.figures
{
    
    public struct Point
    {
        private double x, y;

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
        public double X
        {
            get { return x; }
            set { x = value; }
        }
        public double Y
        {
            get { return y; }
            set { y = value; }
        }
    }
    
    public abstract class Figures
    {
        public char type;
        public abstract double Perimeter();
        public abstract double Area();
        public abstract Point CenterOfMass();
    }

    
    // !!!!!!!! CIRCLE
    public class Circle : Figures
    {
        private Point center;
        private double radius;

        public Circle(Point c, double r)
        {
            this.type = 'c';
            this.center = c;
            this.radius = r;
        }
        public override double Area()
        {
            return Math.PI * Math.Pow(radius,2);
        }
        public override Point CenterOfMass()
        {
            return center;
        }
        public override double Perimeter()
        {
            return Math.PI * radius;
        }
    }
    
    //!!!!!!!! ELLIPSE
    public class Ellipse : Figures
    {
        private Tuple<Point, Point> focuses;
        private double bigAxis, smallAxis;

        public Ellipse(Point x, Point y, double a)
        {
            focuses = new Tuple<Point, Point>(x,y);
            bigAxis = a;
            smallAxis = Math.Sqrt(((Math.Sqrt(Math.Pow(focuses.Item1.X - focuses.Item2.X,2)
                                       +Math.Pow(focuses.Item1.Y - focuses.Item2.Y,2)))/2) - Math.Pow(a,2));
        }
        public override double Area()
        {
            return Math.PI * bigAxis * smallAxis;
        }
        public override Point CenterOfMass()
        {
            double x = focuses.Item2.X - focuses.Item1.X;
            double y = focuses.Item2.X - focuses.Item1.Y;
            return new Point(x,y);
        }
        public override double Perimeter()
        {
            return (Math.PI / 2) * Math.Sqrt(2 * (Math.Pow(bigAxis, 2) + Math.Pow(smallAxis, 2)));
        }
    }
    
    // !!!!! POLYGON
    public class Polygon : Figures
    {
        private Point[] Points;
        private int num;
        
        public Polygon(int N)
        {
            this.num = 0;
            Points = new Point[N];
        }

        public void AddPoint(Point newPoint)
        {
            Points[num] = newPoint;
            num++;
        }
        
        
        public override double Area()
        {
            double S = 0;
            for (int i = 0; i < num; i++)
                S += (Points[(i + 1) % num].X - Points[i].X) * (Points[(i + 1) % num].Y + Points[i].Y);
            return Math.Abs(S/2);
        }
        public override Point CenterOfMass()
        {
            //float P = Perimeter();
            double X = 0;
            for (int i = 0; i < num; i++) X += Points[i].X;
            double Y = 0;
            for (int i = 0; i < num; i++) Y += Points[i].Y;
            
            X /= num;
            Y /= num;
            return new Point(X,Y);
        }
        public override double Perimeter()
        {
            double P = 0;
            for (int i = 0; i < num; i++)
                P += Math.Sqrt( Math.Pow(Points[(i+1)%num].X - Points[i].X,2)
                                        + Math.Pow(Points[(i+1)%num].Y - Points[i].Y,2));
            return P;
        }
    }
}
