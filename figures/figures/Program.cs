﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using figures.figures;
// спрятать функции , перевести все на double , readkey
namespace figures
{
    internal class Program
    {
        private static bool CreateCircle(string s, FiguresList list)
        {
            try
            {
                String[] helper = s.Split(' ');
                
                double x = Convert.ToDouble(helper[0]);
                double y = Convert.ToDouble(helper[1]);
                double r = Convert.ToDouble(helper[2]);
                list.Add(new Circle(new Point(x, y), r));
    
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static bool CreateEllipse(string s, FiguresList list)
        {
            try
            {
                String[] helper = s.Split(' ');

                double x1 = Convert.ToDouble(helper[0]);
                double y1 = Convert.ToDouble(helper[1]);
                double x2 = Convert.ToDouble(helper[2]);
                double y2 = Convert.ToDouble(helper[3]);
                double bigAxis = Convert.ToDouble(helper[4]);
                list.Add(new Ellipse(new Point(x1, y1), new Point(x2, y2), bigAxis));
                return true;
            }
            catch
            {
                return false;
            }          
        }

        private static bool CreatePollygon(string s, FiguresList list)
        {
            try
            {
                String[] helper = s.Split(' ');
                
                int N = Convert.ToInt32(helper[0]);
                Polygon p = new Polygon(N);
                
                for (int i = 1; i < N * 2; i += 2)
                    p.AddPoint(new Point(Convert.ToDouble(helper[i]),Convert.ToDouble(helper[i + 1])));

                list.Add(p);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void Main(string[] args)
        {
            FiguresList list = new FiguresList();
            
            bool isCreating = false;
            
            Console.WriteLine("C - start to create figures\n" +
                              "H - help\n" +
                              "Q - quit\n");
            while (true)
            {
                
                if (!isCreating)
                    switch (Console.ReadKey().Key)
                    {
                        case ConsoleKey.C:
                        {
                            Console.WriteLine();
                            isCreating = true;
                            Console.Write("    E - Ellipse (focuses + little axis + big axxis)\n" +
                                              "    C - Circle ( Center (x,y) + Radius\n" +
                                              "    P - Pollygon ( num of points + points(x,y)\n" +
                                              "    Q - cancel\n" +
                                              "    L - print all figures list\n" +
                                              "    D - delete last figure\n");
                            break;
                        }
                        case ConsoleKey.H:
                        {
                            Console.Write("C - start to create figures\n" +
                                              "H - help\n" +
                                              "Q - quit\n");
                            
                            break;
                        }
                        case ConsoleKey.Q:
                        {
                            return;
                        }
                        default:
                        {
                            Console.Write("C - start to create figures\n" +
                                              "H - help\n" +
                                              "Q - quit\n");
                            break;
                        }
                    }
                else
                    switch (Console.ReadKey().Key)
                    {
                        case ConsoleKey.C:
                        {
                            Console.WriteLine();
                            if (CreateCircle(Console.ReadLine(), list))
                                Console.WriteLine("Circle created");
                            else
                                Console.WriteLine("Bad input(circle)");
                            break;
                        }
                        case ConsoleKey.E: // 
                        {
                            Console.WriteLine();
                            if(CreateEllipse(Console.ReadLine(),list))
                                Console.WriteLine("Ellipse created");
                            else
                                Console.WriteLine("Bad input(ellipse)");
                            
                            break;
                        }
                        case ConsoleKey.P: // create polygon
                        {
                            Console.WriteLine();
                            if(CreatePollygon(Console.ReadLine(),list))
                                Console.WriteLine("Pollygon created");
                            else
                                Console.WriteLine("Bad input(pollygon)");
                            break;
                        }
                        case ConsoleKey.D:
                        {
                            Console.WriteLine();
                            list.DelLast();
                            break;
                        }
                        case ConsoleKey.Q: // cancel
                        {
                            Console.WriteLine();
                            isCreating = false;
                            Console.Write("C - start to create figures\n" +
                                          "H - help\n" +
                                          "Q - quit\n");
                            break;
                        }
                        case ConsoleKey.L: // print info about all figures
                        {
                            Console.WriteLine();
                            list.AllInfo();
                            break;
                        }
                        case ConsoleKey.H: // help info
                        {
                            Console.Write("    E - Ellipse (focuses + little axis + big axxis)\n" +
                                          "    C - Circle ( Center (x,y) + Radius\n" +
                                          "    P - Pollygon ( num of points + points(x,y)" +
                                          "    Q - cancel\n" +
                                          "    L - print all figures list\n" +
                                          "    D - delete last figure\n");
                            break;
                        }
                        default:
                        {
                            Console.Write("    E - Ellipse (focuses + little axis + big axxis)\n" +
                                          "    C - Circle ( Center (x,y) + Radius\n" +
                                          "    P - Pollygon ( num of points + points(x,y)" +
                                          "    Q - cancel\n" +
                                          "    L - print all figures list\n" +
                                          "    D - delete last figure\n");
                            break;
                        }
                    }
                }
        }
    }
}