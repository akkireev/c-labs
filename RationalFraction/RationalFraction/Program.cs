﻿using System;
using System.Collections.Generic;
using RationalFraction.RationalFraction;

namespace RationalFraction
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            
            Ratio ratio1 = new Ratio(6,9);
            Ratio ratio2 = new Ratio(5,6);

            //ratio1=ratio1/ratio2;
            ratio1 = ratio1 * ratio2;
            Console.WriteLine(ratio1.ToString());
            ratio1++;
            Console.WriteLine(ratio1.ToString());
            
            Console.WriteLine(ratio1.ToDouble());
        }
        
        
    }
}