﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime;

namespace RationalFraction.RationalFraction
{
    public class Ratio
    {
        public Ratio(double numerator, double denominator)
        {
            if (denominator == 0)
                throw new System.ArgumentOutOfRangeException("denominator must not be 0");
            
            this.denominator = denominator;
            this.numerator = numerator;
            reduce(gcd(this.denominator,this.numerator));
        }
        
        public static Ratio operator+(Ratio a,Ratio b)
        {
            double lcm = LCM(a.denominator, b.denominator);
            double forA = a.denominator / lcm;
            double forB = b.denominator / lcm;
            
            return new Ratio(a.numerator*forA + b.numerator*forB , a.denominator*lcm);
        }
        public static Ratio operator-(Ratio a,Ratio b)
        {
            double lcm = LCM(a.denominator, b.denominator);
            double forA = a.denominator / lcm;
            double forB = b.denominator / lcm;
            
            return new Ratio(a.numerator*forA - b.numerator*forB , a.denominator*lcm);
        }
        public static Ratio operator*(Ratio a,Ratio b)
        { 
            return new Ratio(a.numerator*b.numerator, a.denominator*b.denominator);
        }
        public static Ratio operator/(Ratio a,Ratio b)
        { 
            return new Ratio(a.numerator*b.denominator, a.denominator*b.numerator);
        }
        public double ToDouble()
        {
            return numerator / denominator;
        }
        public static Ratio operator ++(Ratio a)
        {
            a.numerator += a.denominator;
            
            return new Ratio(a.numerator,a.denominator);
        }
        public static Ratio operator --(Ratio a)
        {
            a.numerator -= a.denominator;
            
            return new Ratio(a.numerator,a.denominator);
        }
        override public string ToString()
        {
            if (numerator == 0)
                return "0";
            else if (numerator == denominator)
                return denominator.ToString();
            else
                return numerator + "/" +denominator;

        }
        
        
        private double gcd(double a, double b)
        {
            return (b != 0) ? gcd(b, a % b) : a;
        }
        private void reduce(double g)
        {
            if (g == 1)
                return;
            denominator /= g;
            numerator /= g;
        }
        private static double LCM(double a, double b)
        {
            double n;
            double max = (a > b) ? a : b;
            for(double i=max;i<(a*b+1);i++)
                if (i % a == 0 && i % b == 0)
                    return i;

            return 0;
        }
        
        private double denominator;
        private double numerator;
    }
}