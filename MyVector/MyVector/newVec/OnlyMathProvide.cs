﻿using System;

namespace MyVector.newVec
{
    public interface IOperations<T>
    {
        T Add(T a, T b);
        T Subtract(T a, T b);
        T Multiply(T a, T b);
        T Divide(T a, T b);
    }

    class ByteOperations : IOperations<byte>
    {
        public byte Add(byte a, byte b)      { return unchecked ((byte)(a + b)); }
        public byte Subtract(byte a, byte b) { return unchecked ((byte)(a - b)); }
        public byte Multiply(byte a, byte b) { return unchecked ((byte)(a * b)); }
        public byte Divide(byte a, byte b)   { return unchecked ((byte)(a / b)); }
    }

    class DoubleOperations : IOperations<double>
    {
        public double Add(double a,double b) { return unchecked ((double) (a + b)); }
        public double Subtract(double a, double b) { return unchecked ((double)(a - b)); }
        public double Multiply(double a, double b) { return unchecked ((double)(a * b)); }
        public double Divide(double a, double b)   { return unchecked ((double)(a / b)); }   
    }
        
    class IntOperations : IOperations<int>
    {
        public int Add(int a,int b) { return unchecked ((int) (a + b)); }
        public int Subtract(int a, int b) { return unchecked ((int)(a - b)); }
        public int Multiply(int a, int b) { return unchecked ((int)(a * b)); }
        public int Divide(int a, int b)   { return unchecked ((int)(a / b)); }   
    }
        
    class SingleOperations : IOperations<float>
    {
        public float Add(float a, float b)      { return a + b; }
        public float Subtract(float a, float b) { return a - b; }
        public float Multiply(float a, float b) { return a * b; }
        public float Divide(float a, float b)   { return a / b; }
    }
}