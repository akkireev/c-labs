﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MyVector.MyVec;

namespace MyVector.newVec
{   
    
    public class LastVec<T>
    {
        private static IOperations<T> math;
        
        static LastVec()
        {
                var type = typeof(T);
                switch (Type.GetTypeCode(type))
                {
                    case TypeCode.Double:
                        math = (IOperations<T>) new DoubleOperations();
                        break;
                    case TypeCode.Byte:
                        math =  (IOperations<T>)new ByteOperations();
                        break;
                    case TypeCode.Single:
                        math = (IOperations<T>)new SingleOperations();
                        break;
                    case TypeCode.Int32:
                        math = (IOperations<T>)new IntOperations();
                        break;
                    default:
                        var message = String.Format("Operations for type {0} is not supported.", type.Name);
                        throw new NotSupportedException(message);
                }
        }

        public static LastVec<T> operator+(LastVec<T> fV, LastVec<T> sV)
        {
            CheckLength(fV,sV);
            LastVec<T> next = new LastVec<T>();
            int size = fV.Size;
            for(int i=0;i<size;i++)
                next.PushBack(math.Add(fV[i],sV[i]));

            return next;
        }
        public static LastVec<T> operator-(LastVec<T> fV, LastVec<T> sV)
        {
            CheckLength(fV,sV);
            LastVec<T> next = new LastVec<T>();
            int size = fV.Size;
            for(int i=0;i<size;i++)
                next.PushBack(math.Subtract(fV[i],sV[i]));

            return next;
        }
        public static LastVec<T> operator*(LastVec<T> fV, T con)
        {
            int size = fV.Size;
            for(int i=0;i<size;i++)
                fV[i]=math.Multiply(fV[i],con);

            return fV;
        }
        public static LastVec<T> operator*(LastVec<T> fV, LastVec<T> sV)
        {
            CheckLength(fV,sV);

            switch (fV.Size)
            {
                case 3:
                {
                    LastVec<T> next = new LastVec<T>();
                    
                    next.PushBack(math.Subtract(math.Multiply(fV[1],sV[2]),math.Multiply(fV[2],sV[1])));
                    next.PushBack(math.Subtract(math.Multiply(fV[2], sV[0]),math.Multiply(fV[0], sV[2])));
                    next.PushBack(math.Subtract(math.Multiply(fV[0], sV[1]), math.Multiply(fV[1], sV[0])));

                    return next;
                }
                default:
                {
                    Console.WriteLine("Scale of vector must be equal 3");
                    return fV;
                }
            }
        }
        public T DOT(LastVec<T> fV, LastVec<T> sV) 
        {
            CheckLength(fV,sV);

            T sum = math.Multiply(fV[0],sV[0]);
            int size = fV.Size;
            for (int i = 1; i < size; i++)
                sum = math.Add(sum, math.Multiply(fV[i],sV[i]));

            return sum;
        }
        public void PushBack(T el)
        {            
            data.Add(el);
        }
        public int Size
        {
            get{ return data.Count; }
        }
        public T this[int index]
        {
            get{ return data[index]; }
            set{ data[index] = value; }
        }
        public override string ToString()
        {
            string a = "{ "+ data[0];
            int size = Size;
            for (int i = 1; i < size; i++)
                a +=" , " + data[i];

            a += " }";
            return a;
        }
        
        private static void CheckLength(LastVec<T> fV, LastVec<T> sV)
        {
            if(fV.Size != sV.Size)
                throw new System.ArgumentException("Vectors have different length");
        }
        private List<T> data = new List<T>(); 
    }
}