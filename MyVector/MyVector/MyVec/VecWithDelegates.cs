﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MyVector.MyVec
{
    public delegate T mult<T>(T a, T b);
    public delegate T sum<T>(T a, T b);
    public delegate T dif<T>(T a, T b);
    
    
    public class MyVec<T>
    {
        
        public static MyVec<T> operator+(MyVec<T> fV, MyVec<T> sV)
        {
            CheckLength(fV,sV);
            MyVec<T> next = new MyVec<T>();
            int size = fV.Size;
            for(int i=0;i<size;i++)
                next.PushBack(Add(fV[i],sV[i]));

            return next;
        }
        public static MyVec<T> operator-(MyVec<T> fV, MyVec<T> sV)
        {
            CheckLength(fV,sV);
            MyVec<T> next = new MyVec<T>();
            int size = fV.Size;
            for(int i=0;i<size;i++)
                next.PushBack(Subtract(fV[i],sV[i]));

            return next;
        }
        public static MyVec<T> operator*(MyVec<T> fV, T con)
        {
            int size = fV.Size;
            for(int i=0;i<size;i++)
                fV[i]=Multiply(fV[i],con);

            return fV;
        }
        public static MyVec<T> operator*(MyVec<T> fV, MyVec<T> sV)
        {
            CheckLength(fV,sV);

            switch (fV.Size)
            {
                case 3:
                {
                    MyVec<T> next = new MyVec<T>();
                    
                    next.PushBack(Subtract(Multiply(fV[1],sV[2]),Multiply(fV[2],sV[1])));
                    next.PushBack(Subtract(Multiply(fV[2], sV[0]),Multiply(fV[0], sV[2])));
                    next.PushBack(Subtract(Multiply(fV[0], sV[1]), Multiply(fV[1], sV[0])));

                    return next;
                }
                default:
                {
                    Console.WriteLine("Scale of vector must be equal 3");
                    return fV;
                }
            }
        }
        public T DOT(MyVec<T> fV, MyVec<T> sV) 
        {
            CheckLength(fV,sV);

            T sum = Multiply(fV[0],sV[0]);
            int size = fV.Size;
            for (int i = 1; i < size; i++)
                sum = Add(sum, Multiply(fV[i],sV[i]));

            return sum;
        }
        public void PushBack(T el)
        {            
            data.Add(el);
        }
        public int Size
        {
            get{ return data.Count; }
        }
        public T this[int index]
        {
            get{ return data[index]; }
            set{ data[index] = value; }
        }
        public override string ToString()
        {
            string a = "{ "+ data[0];
            int size = Size;
            for (int i = 1; i < size; i++)
                a +=" , " + data[i];

            a += " }";
            return a;
        }
        
        private static void CheckLength(MyVec<T> fV, MyVec<T> sV)
        {
            if(fV.Size != sV.Size)
                throw new System.ArgumentException("Vectors have different length");
        }
        
        public static mult<T> Multiply;
        public static sum<T> Add;
        public static dif<T> Subtract;
        private List<T> data = new List<T>();
    }
}