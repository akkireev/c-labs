﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Xsl.Runtime;
using MyVector.MyVec;
using MyVector.newVec;

namespace MyVector
{
    
    internal class Program
    {
        private const int size = 3;
        
        public static void Main(string[] args)
        {
            LastVec<double> fVec = new LastVec<double>();
            LastVec<double> sVec = new LastVec<double>();

            for (int i = 0; i < size; i++)
            {
                fVec.PushBack(1);
                sVec.PushBack(2);
            }
            
            
            //fVec[3] = 10;
            //Console.WriteLine(fVec.DOT(fVec, sVec));
            //Console.WriteLine((fVec*sVec).ToString());
            //Console.WriteLine(fVec*20);
            //Console.WriteLine(fVec.ToString());
        }
        
    }

    
    
    /*public class Operations
    {
        public static double multiple(double a, double b)
        {
            return a * b;
        }
        public static double plus(double a, double b)
        {
            return a + b;
        }
        public static double minus(double a, double b)
        {
            return a - b;
        }
    }
    
    
    internal class Program
    {
        
        public static void Main(string[] args)
        {
            const int size = 3;
            // Operations created by user
            MyVec<double>.Multiply = Operations.multiple;
            MyVec<double>.Add = Operations.plus;
            MyVec<double>.Subtract = Operations.minus;
            
            
            MyVec<double> fVec = new MyVec<double>();
            MyVec<double> sVec = new MyVec<double>();

            for (int i = 0; i < size; i++)
            {
                fVec.PushBack(1);
                sVec.PushBack(2);
            }
            
            
            //fVec[3] = 10;
            //Console.WriteLine(fVec.DOT(fVec, sVec));
            //Console.WriteLine((fVec*sVec).ToString());
            //Console.WriteLine(fVec*20);
            //Console.WriteLine(fVec.ToString());
        }
    }*/
}