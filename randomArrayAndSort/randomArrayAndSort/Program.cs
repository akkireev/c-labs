﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace randomArrayAndSort
{
    internal class Program
    {
        public class ReverseComparer : IComparer  
        {
            // Call CaseInsensitiveComparer.Compare with the parameters reversed.
            public int Compare(Object x, Object y)  
            {
                return (new CaseInsensitiveComparer()).Compare(y, x );
            }
        }
        public static void Main(string[] args)
        {
            Random rnd = new Random();
            int size = rnd.Next(10, 15);
            int[] arr = new int[size];
            for (int i = 0; i < size; i++)
            {
                arr[i] = rnd.Next(-100, 100);
            }
      
            for(int i=0;i<size;i++)
                Console.WriteLine(arr[i] + " ");
            Console.Write("\n\n");

            int[] arr2 = arr;
            Array.Sort(arr,Comparer.Default);
            //Array.Sort(arr);
            for(int i=0;i<size;i++)
                Console.WriteLine(arr[i] + " == " + arr2[i] + " ");
            
            Console.WriteLine("\n" + arr.Equals(arr2));
        }
    }
}